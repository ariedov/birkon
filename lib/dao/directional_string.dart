import 'package:flutter/material.dart';

class DirectionalString {

  final String text;
  final TextDirection direction;

  DirectionalString(this.text, this.direction);
}